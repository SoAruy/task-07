package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance = null;

    //    private static String CONNECTION_URL = "jdbc:mysql://localhost:3306/test2db?user=root&password=root";
    private static String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
    private static String ADD_USER = "INSERT INTO users (login) VALUES(?)";
    private static String SELECT_ALL_USERS = "SELECT * FROM users ORDER BY id";
    private static String ADD_TEAM = "INSERT INTO teams (name) VALUES(?)";
    private static String SELECT_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static String GET_USER = "SELECT * FROM users WHERE login = ?";
    private static String GET_TEAM = "SELECT * FROM teams WHERE name = ?";
    private static String SET_TEAM_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES(?, ?)";
    private static String SELECT_USER_TEAMS = "SELECT * FROM teams JOIN users_teams ON users_teams.team_id = teams.id WHERE users_teams.user_id = ?";
    private static String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    private static String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE name = ?";

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> allUsersList = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#findAllUsers");

            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_USERS);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String login = resultSet.getString("login");

                User user = new User(id, login);

                allUsersList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allUsersList;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#insertUser");

            PreparedStatement preparedStatement = con.prepareStatement(ADD_USER);
            preparedStatement.setString(1, user.getLogin());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        return false;
    }

    public User getUser(String login) throws DBException {
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#getUser");
            PreparedStatement preparedStatement = con.prepareStatement(GET_USER);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return new User(resultSet.getInt("id"), resultSet.getString("login"));
            else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#getTeam");
            PreparedStatement preparedStatement = con.prepareStatement(GET_TEAM);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return new Team(resultSet.getInt("id"), resultSet.getString("name"));
            else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> allTeamsList = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#findAllTeams");

            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_TEAMS);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");

                Team team = new Team(id, name);

                allTeamsList.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allTeamsList;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#insertTeam");

            PreparedStatement preparedStatement = con.prepareStatement(ADD_TEAM);
            preparedStatement.setString(1, team.getName());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        try {
            System.out.println("#setTeamsForUsers");
            con = DriverManager.getConnection(CONNECTION_URL);
            con.setAutoCommit(false);
            int userId = getInstance().getUser(user.getLogin()).getId();

            PreparedStatement preparedStatement = con.prepareStatement(SET_TEAM_FOR_USER);
            for (Team team : teams) {
                int teamId = getInstance().getTeam(team.getName()).getId();
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, teamId);
                preparedStatement.executeUpdate();
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#getUserTeams");
            int userId = getInstance().getUser(user.getLogin()).getId();

            PreparedStatement preparedStatement = con.prepareStatement(SELECT_USER_TEAMS);
            preparedStatement.setInt(1, userId);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");

                Team team = new Team(id, name);

                userTeams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ;
        return userTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#deleteTeam");
            int teamId = getInstance().getTeam(team.getName()).getId();

            PreparedStatement preparedStatement = con.prepareStatement(DELETE_TEAM);
            preparedStatement.setInt(1, teamId);

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(CONNECTION_URL)) {
            System.out.println("#updateTeam");

            PreparedStatement preparedStatement = con.prepareStatement(UPDATE_TEAM);
            preparedStatement.setString(1, team.getName());
            preparedStatement.setString(2, team.getOldName());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
