package com.epam.rd.java.basic.task7;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.epam.rd.java.basic.task7.db.*;
import com.epam.rd.java.basic.task7.db.entity.*;

public class Demo {

	private static void print(List<?> list) {
		list.forEach(System.out::println);
	}

	private static <T, U extends Comparable<? super U>> List<T>
	sort(List<T> items, Function<T, U> extractor) {
		items.sort(Comparator.comparing(extractor));
		return items;
	}

	public static void main(String[] args) throws DBException {
		DBManager dbm = DBManager.getInstance();
		User user = User.createUser("user");
		Team teamA = Team.createTeam("A");
		Team teamB = Team.createTeam("B");

		dbm.insertUser(user);
		dbm.insertTeam(teamA);
		dbm.insertTeam(teamB);

		dbm.setTeamsForUser(user, teamA, teamB);

		teamA.setName("Z");
		dbm.updateTeam(teamA);

		List<Team> teams = sort(dbm.getUserTeams(user), Team::getName);
		System.out.println(teams);
//		// users  ==> [ivanov]
//		// teams  ==> [teamA]
//
//		DBManager dbManager = DBManager.getInstance();
//
//		// Part 1
//		dbManager.insertUser(User.createUser("petrov"));
//		dbManager.insertUser(User.createUser("obama"));
//
//		dbManager.findAllUsers().forEach(System.out::println);
//		print(dbManager.findAllUsers());
//		// users  ==> [ivanov, petrov, obama]
//
//		System.out.println("===========================");
//
//		// Part 2
//		dbManager.insertTeam(Team.createTeam("teamB"));
//		dbManager.insertTeam(Team.createTeam("teamC"));
//
//		print(dbManager.findAllTeams());
//		// teams ==> [teamA, teamB, teamC]
//
//		System.out.println("===========================");
//
//		// Part 3
//		User userPetrov = dbManager.getUser("petrov");
//		User userIvanov = dbManager.getUser("ivanov");
//		User userObama = dbManager.getUser("obama");
//
//		Team teamA = dbManager.getTeam("teamA");
//		Team teamB = dbManager.getTeam("teamB");
//		Team teamC = dbManager.getTeam("teamC");
//
//
//		// method setTeamsForUser must implement transaction!
//		dbManager.setTeamsForUser(userIvanov, teamA);
//		dbManager.setTeamsForUser(userPetrov, teamA, teamB);
//		dbManager.setTeamsForUser(userObama, teamA, teamB, teamC);
//
//		for (User user : dbManager.findAllUsers()) {
//			print(dbManager.getUserTeams(user));
//			System.out.println("~~~~~");
//		}
//		// teamA
//		// teamA teamB
//		// teamA teamB teamC
//
//		// Part 4
//		// on delete cascade!
//		dbManager.deleteTeam(teamA);
//
//		// Part 5
//		teamC.setName("teamX");
//		dbManager.updateTeam(teamC);
//		print(dbManager.findAllTeams());
//		// teams ==> [teamB, teamX]
//
//		System.out.println("===========================");
//
//		// Part 6
//		dbManager.deleteUsers(dbManager.findAllUsers().toArray(User[]::new));
//		for (Team team : dbManager.findAllTeams()) {
//			dbManager.deleteTeam(team);
//		}
//
//		dbManager.insertUser(User.createUser("ivanov"));
//		dbManager.insertTeam(Team.createTeam("teamA"));
//
//		print(dbManager.findAllTeams());
//		// teams ==> [teamA]
//
//		print(dbManager.findAllUsers());
//		// users ==> [ivanov]
	}

}
